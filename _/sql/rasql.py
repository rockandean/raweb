#!/usr/bin/env python
print "Content-type:text/html\n\n"
br="</br>"

# INDEX
    # Import modules
    # Table creation
    # function declaration
#

### import modules
try:
    import MySQLdb
    import sys
except ImportError, e:
    print e

try:
    db = MySQLdb.connect (
    host = "rockandeancom.ipagemysql.com",
    user = "rockandean",
    passwd = "Gg21ghb38577",
    db = "ra_")
    print "connected to the database"+br
except MySQLdb.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1]), br
    sys.exit (1)

### Table creation
# Use a new dictionary each time
Users={
    "name":"USERS",
    "keys" : ["FirstName","LastName","Age","Sex",
              "Income"],
    "types": ["varchar(255) NOT NULL", "varchar(255)",
              "varchar(255)","char(1)","varchar(255)"] }
comma=', '
keys=Users.keys()
mk=Users["keys"]
mt=Users["types"]
l=[(x,y ) for x,y in zip(mk,mt)]
table=', '.join('%s %s' % (t[0],t[1]) for t in l)
values=', '.join('%s' % (t[0]) for t in l)

### Function definition, historical order
def wrap(foo,_s=""):
    """
    wrap('text')
    wrap('text',"")
    """
    # simple add '('+ foo +')'
    return "("+_s+ foo +_s+")"
    
def version(_db):
    """ prepare cursor object """
    """ print version of SQL"""
    cursor = _db.cursor()
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    print "Database VERSION: %s" %data, br

def createTable(_db):
    """ Create a New database table, SQL is NOT case sensitive """
    sql = "CREATE TABLE " + Users["name"] + wrap(table)
    cursor =_db.cursor()
    dropTable="DROP TABLE IF EXISTS "+Users["name"] # drop delete the table
    cursor.execute(dropTable)
    try:
        cursor.execute(sql)
        print "table created", br
    except MySQLdb.Error, e:
        print "Error %d: %s" %(e.args[0], e.args[1]), br
        sys.exit(1)

def insert(_db,_myValues):
    """
    insert()
    SQL query to insert a record to database
    _myValues is a tuple
    """
    _myval='", "'.join('%s' % (t) for t in _myValues)
    sql="INSERT INTO " + Users["name"] +" "+ wrap(values)+" Values "+wrap(_myval,'"')
    cursor=_db.cursor()
    try:
        cursor.execute(sql)
        print "New record successful ", br
    except MySQLdb.Error, e:
        print "Error %d: %s" %(e.args[0], e.args[1])

def email(_password):
    try:
        import smtplib
        import base64
        print "hi from smtplib:", br
    except:
        pass
    try:
        filename = "./tmp/test.txt"
        # Read a file and encode it into base64 format
        fo = open(filename, "rb")
        filecontent = fo.read()
        encodedcontent = base64.b64encode(filecontent)  # base64    
    except:
        print 'Error reading file', br

    username="admin@rockandean.com"
    password=_password
    sender = 'admin@rockandean.com'
    receivers = ['fcm007@gmail.com','poloacho@gmail.com']
    
    marker = 'ROCKANDEANMARKER'
    body ="""This is a test email to send an attachement."""
    songLink='https://www.youtube.com/watch?v=6FBJkYIve3o'
    bandName='Kalamarka'
    songName='he venido'
    htmlMsg = """<h1>RockAndean</h1>
<p>Dear Leo,</p>
<p>Enjoy the masterful pieces of rock classics
 and the ancient traditions of andean folclore</p>
<p><a href="https://www.youtube.com/watch?v=dTaD9cd8hvw">Iron Maiden: the troopers!</a></p>
<p></p>
<p><a href="%s">%s: %s!</a></p>
<p>Long live RockAndean</p>
""" % (songLink, bandName, songName)
    # Define the main headers.
    part1 = """From: RockAndean <admin@rockandean.com>
To: Leo Carbajal <poloacho@gmail.com>
Subject: Sending Attachement
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary=%s
--%s
""" % (marker, marker)
    part2 = """Content-Type: text/html
%s
--%s
""" %(htmlMsg, marker)
    # Define the message action
    part3 = """Content-Type: text/plain
Content-Transfer-Encoding: 8bit
%s
%s
--%s
""" % (body, songLink, marker)

    # Define the attachment section
    part4 = """Content-Type: multipart/mixed; name=\"%s\"
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename=%s

%s
--%s--""" %(filename, filename, encodedcontent, marker)

    message = part1 + part2 + part3 + part4 
    print message
    try:
       server = smtplib.SMTP('rockandeancom.ipage.com',587)
       server.set_debuglevel(False)
       server.login(user=username, password=password)
       try:
           server.sendmail(sender, receivers, message)      
           print "Successfully sent email"
       except:
            print 'Error Sending Email'
       finally:
           server.close()
    except smtplib.SMTPException, e:
       print "Error: unable to send email", e
       sys.exit()

### Main Program 
version(db)
createTable(db)
myValues=['Mac', 'Mohan', '20', 'M', '2000']
insert(db,myValues)
email("6USzCE28rKFcRr")

db.close()   

