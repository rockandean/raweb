MathJax.Hub.Config({skipStartupTypeset: true});
MathJax.Hub.Configured();

var myApp = angular.module("myApp", []);

myApp.directive("mathjaxBind", function() {
    return {
        restrict: "A",
        controller: ["$scope", "$element", "$attrs",
                function($scope, $element, $attrs) {
            $scope.$watch($attrs.mathjaxBind, function(texExpression) {
                var texScript = angular.element("<script type='math/tex; mode:display'>")
                    .html(texExpression ? texExpression :  "");
                $element.html("");
                $element.append(texScript);
                MathJax.Hub.Queue(["Reprocess", MathJax.Hub, $element[0]]);
            });
        }]
    };
});
function MyCtrl($scope, $element) {
    $scope.expression = "\\frac{5}{4} \\div \\frac{1}{6}\\\\ \n\\int f(x) dx\\\\ \n\\sum (\\frac{x^2}{M_{\\odot}})";
}


// handle onclick emailEqButton

document.getElementById("emailEqButton").onclick = function () {
    document.getElementById("emailReply").innerHTML = "email Sent!";
}

function printEmail($scope){
    var mymsg = $scope.expression
}